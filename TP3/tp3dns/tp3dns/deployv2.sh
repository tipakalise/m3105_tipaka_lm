#!/usr/bin/env bash

#Execution du script bash permettant de configurer les autres équipements
bash deploy.sh

#Eteindre les serveur DNS
himage dza killall -9 -q named
himage deduza killall -9 -q named
himage dcomza killall -9 -q named
himage duniveduza killall -9 -q named
himage dfacebookcomza killall -9 -q named
himage aRootServer killall -9 -q named
himage bRootServer killall -9 -q named
himage cRootServer killall -9 -q named
himage dre killall -9 -q named
himage dorg killall -9 -q named
himage dorgslave killall -9 -q named
himage dwikiorg killall -9 -q named
himage drtiutre killall -9 -q named
himage diutre killall -9 -q named

#Nouvelle configuration du serveur root DNS aRootServer
himage aRootServer rm /etc/named.conf
himage aRootServer rm /etc/named/root
himage aRootServer rm /etc/named/in-addr.arpa
hcp SecondePartieConfiguration/aRootServer/named.conf aRootServer:/etc/.
hcp SecondePartieConfiguration/aRootServer/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf
himage aRootServer named -c /etc/named.conf


#Configuration du serveur root DNS bRootServer
himage bRootServer mkdir -p /etc/named
hcp SecondePartieConfiguration/bRootServer/named.conf bRootServer:/etc/.
hcp SecondePartieConfiguration/bRootServer/localhost.rev bRootServer:/etc/named/.
himage bRootServer named -c /etc/named.conf

#Configuration du serveur root DNS cRootServer
himage cRootServer mkdir -p /etc/named
hcp SecondePartieConfiguration/cRootServer/named.conf cRootServer:/etc/.
hcp SecondePartieConfiguration/cRootServer/localhost.rev cRootServer:/etc/named/.
himage cRootServer named -c /etc/named.conf

#Nouvelle configuration du serveur DNS .re
himage dre rm /etc/named/named.root
hcp SecondePartieConfiguration/dre/* dre:/etc/named/.
himage dre named -c /etc/named.conf

#Nouvelle configuration du serveur DNS .org
himage dorg rm /etc/named/named.root
hcp SecondePartieConfiguration/dorg/* dorg:/etc/named/.
hcp SecondePartieConfiguration/dorg/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf 
himage dorg named -c /etc/named.conf

#Configuration du serveur DNS secondaire de org
himage dorgslave mkdir -p /etc/named
hcp SecondePartieConfiguration/dorgslave/named.conf dorgslave:/etc/.
hcp SecondePartieConfiguration/dorgslave/* dorgslave:/etc/named/.
himage dorgslave rm /etc/named/named.conf 
himage dorgslave named -c /etc/named.conf

#Configuration du serveur DNS .za
himage dza mkdir -p /etc/named
hcp SecondePartieConfiguration/dza/named.conf dza:/etc/.
hcp SecondePartieConfiguration/dza/* dza:/etc/named/.
himage dza rm /etc/named/named.conf 
himage dza named -c /etc/named.conf

#Nouvelle configuration du serveur DNS wiki.org
himage dwikiorg rm /etc/named/named.root
hcp SecondePartieConfiguration/dwikiorg/* dwikiorg:/etc/named/.
himage dwikiorg named -c /etc/named.conf

#Nouvelle configuration du serveur DNS iut.re
himage diutre rm /etc/named/named.root
hcp SecondePartieConfiguration/diutre/* diutre:/etc/named/.
himage diutre named -c /etc/named.conf

#Nouvelle configuration du serveur DNS rt.iut.re
himage drtiutre rm /etc/named/named.root
hcp SecondePartieConfiguration/drtiutre/* drtiutre:/etc/named/.
himage drtiutre named -c /etc/named.conf

#Configuration du serveur DNS com.za
himage dcomza mkdir -p /etc/named
hcp SecondePartieConfiguration/dcomza/named.conf dcomza:/etc/.
hcp SecondePartieConfiguration/dcomza/* dcomza:/etc/named/.
himage dcomza rm /etc/named/named.conf 
himage dcomza named -c /etc/named.conf

#Configuration du serveur DNS edu.za
himage deduza mkdir -p /etc/named
hcp SecondePartieConfiguration/deduza/named.conf deduza:/etc/.
hcp SecondePartieConfiguration/deduza/* deduza:/etc/named/.
himage deduza rm /etc/named/named.conf 
himage deduza named -c /etc/named.conf

#Configuration du serveur DNS facebook.com.za
himage dfacebookcomza mkdir -p /etc/named
hcp SecondePartieConfiguration/dfacebookcomza/named.conf dfacebookcomza:/etc/.
hcp SecondePartieConfiguration/dfacebookcomza/* dfacebookcomza:/etc/named/.
himage dfacebookcomza rm /etc/named/named.conf 
himage dfacebookcomza named -c /etc/named.conf

#Configuration du serveur DNS univ.edu.za
himage duniveduza mkdir -p /etc/named
hcp SecondePartieConfiguration/duniveduza/named.conf duniveduza:/etc/.
hcp SecondePartieConfiguration/duniveduza/* duniveduza:/etc/named/.
himage duniveduza rm /etc/named/named.conf 
himage duniveduza named -c /etc/named.conf

#Mise en place de serveur web sur les machines www
#himage www mkdir -p /tmp/www
#hcp SecondePartieConfiguration/www/index.html www:/tmp/www/.
#himage www python -m SimpleHTTPServer 80

#Configuration du fichier resolv.conf pour les pc
hcp SecondePartieConfiguration/pc3/resolv.conf pc3:/etc/.
hcp SecondePartieConfiguration/pc4/resolv.conf pc4:/etc/.
hcp SecondePartieConfiguration/pc5/resolv.conf pc5:/etc/.
hcp SecondePartieConfiguration/pc6/resolv.conf pc6:/etc/.

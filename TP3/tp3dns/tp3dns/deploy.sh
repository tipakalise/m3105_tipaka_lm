#!/usr/bin/env bash

#Disposition du clavier en AZERTY
setxkbmap -layout fr 

#Eteindre tous les serveurs DNS
himage dwikiorg killall -9 -q named
himage diutre   killall -9 -q named
himage dorg killall -9 -q named
himage dre  killall -9 -q named
himage aRootServer  killall -9 -q named

#Configuration du serveur DNS wiki.org
himage dwikiorg mkdir -p /etc/named
hcp dwikiorg/named.conf dwikiorg:/etc/.
hcp dwikiorg/* dwikiorg:/etc/named/.
himage dwikiorg rm /etc/named/named.conf 
himage dwikiorg named -c /etc/named.conf

#Configuration du serveur DNS iut.re
himage diutre mkdir -p /etc/named
hcp diutre/named.conf diutre:/etc/.
hcp diutre/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf 
himage diutre named -c /etc/named.conf

#Configuration du serveur DNS rt.iut.re
himage drtiutre mkdir -p /etc/named
hcp drtiutre/named.conf drtiutre:/etc/.
hcp drtiutre/* drtiutre:/etc/named/.
himage drtiutre rm /etc/named/named.conf 
himage drtiutre named -c /etc/named.conf

#Configuration du serveur DNS .org
himage dorg mkdir -p /etc/named
hcp dorg/named.conf dorg:/etc/.
hcp dorg/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf 
himage dorg named -c /etc/named.conf

#Configuration du serveur DNS .re
himage dre mkdir -p /etc/named
hcp dre/named.conf dre:/etc/.
hcp dre/* dre:/etc/named/.
himage dre rm /etc/named/named.conf 
himage dre named -c /etc/named.conf

#Configuration du serveur DNS .aRootServer
himage aRootServer mkdir -p /etc/named
hcp aRootServer/named.conf aRootServer:/etc/.
hcp aRootServer/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf 
himage aRootServer named -c /etc/named.conf

#Configuration du fichier resolv.conf pour pc1
hcp pc1/resolv.conf pc1:/etc/.

#Configuration du fichier resolv.conf pour pc2
hcp pc2/resolv.conf pc2:/etc/.